## Вариант 3

1)	Докажите, что в графе, между любыми двумя вершинами которого существует не менее двух непересекающихся простых путей, любые две вершины принадлежат некоторому простому циклу.
Слишком просто! Просто фиксируем пару вершин. Они в простом цикле => существует не менее двух непересекающихся простых путей (по часовой и против). Ч.Т.Д.
2)	Напишите функцию находящую сумму длин кратчайших путей между вершиной “0" и остальными вершинами в связном графе, заданном списком смежности. Функция должна принимать в себя размер графа, указатель на список смежности, и возвращать одно целое значение - сумма длин кратчайших путей (количество ребер).
```c++
int aa(int** G, int N) {
  vector<vector<int>> mat(n, vector<int>(n));
	for (int i = 0; i < n*(n - 1) / 2; i++) {
		int a, b, c;
		cin >> a >> b>>c;
		a--; b--;
		mat[a][b] = mat[b][a] = c;
  }
  int s = 0
	for (int k = 0; k < n; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
        mat[i][j] = max(mat[i][j], mat[i][k] + mat[k][j]);
  for (int j = 0; j < n; j++)
    s += mat[a][j];
  return s;
}
```
3)	Напишите программу находящую сумму степеней всех вершин являющихся шарнирами. Программа принимает в себя список ребер графа.
```c++
#include <bits/stdc++.h>
using namespace std;

class CutVertex {
	void dfs(int s, int p, int &t, vector<vector<int>> &g, vector<int> &tin,
		vector<int> &up, vector<int> &ans) {
		tin[s] = up[s] = t++;
		int v = 0;
		for (int to : g[s]) {
			if (p == to) continue;
			if (!tin[to]) {
				v++;
				dfs(to, s, t, g, tin, up, ans);
				up[s] = min(up[s], up[to]);
			}
			else up[s] = min(up[s], tin[to]);
		}
		if (p != -1) {
			for (int to : g[s]) {
				if (up[to] >= tin[s]) {
					ans.push_back(s);
					break;
				}
			}
		}
		if (p == -1 && v > 1) ans.push_back(s);
	}
public:
	CutVertex() { }
	vector<int> find(vector<vector<int>> &g) {
		int n = g.size();
		vector<int> tin(n), up(n), ans;
		int t = 1;
		for (int i = 0; i < n; i++) {
			if (tin[i] == 0) dfs(i, -1, t, g, tin, up, ans);
		}
		return ans;
	}
	vector<int> operator ()(vector<vector<int>> &g) {
		return find(g);
	}
};

int main() {
	int n, m;
	cin >> n >> m;
	vector<vector<int>> g(n);
	for (int i = 0; i < m; i++) {
		int a, b;
		cin >> a >> b;
		a--; b--;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	CutVertex cv;
  vector<int> ans = cv(g);
  int s = 0;
	for (int i = 0; i < ans.size(); i++)
      s+=g[ans[i]].size();
  cout<< s;
}	
```
4)	В службе  грузоперевозок города N кадровые перестановки. Водитель Арсен переведен в статистический отдел, работников которого уволили за махинации с накладными расходами. Арсен собрал информацию о средней стоимости ремонта грузовика после проезда между пригородными селами по плохим дорогам. Напишите программу, находящую стоимости ремонта машин при поездке из любого села в любое, при условии что машины будут ездить максимально безопасными маршрутами. Формат входных данных остается на Ваше усмотрение.
```c++
#include <bits/stdc++.h>
using namespace std;
int main() {
	int n;
	cin >> n;
	vector<vector<int>> mat(n, vector<int>(n));
	for (int i = 0; i < n*(n - 1) / 2; i++) {
		int a, b,c;
		cin >> a >> b>>c;
		a--; b--;
		mat[a][b] = mat[b][a] = c;
	}
	for (int k = 0; k < n; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
	
	int s = 0;
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < n; j++) 
			s+=mat[i][j];
	cout << s;
}
```

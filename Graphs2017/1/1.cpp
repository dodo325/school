#include <vector>
#include <iostream>
using namespace std;

void dfs(int s, vector<vector<int>> &g, vector<int> &c, int cur) { // поиск в высоту
    c[s] = cur;
    for (int to : g[s]) {
        if (c[to] == 0) { // не ходим там, где были
            dfs(to, g, c, cur);
        }
    }
}
int main() {
    int n, m; // n - кол. вершин, m - кол. рёбер
    cin >> n >> m;
    vector<vector<int>> g(n); // список смежности
    for (int i = 0; i < m; i++) {// вводим список рёбер
        int a, b;
        cin >> a >> b;
        a--; b--; // нумерация вершин с 1 !!!
        g[a].push_back(b);
        g[b].push_back(a);
    }
    vector<int> c(n); // массив с номерами компонент связности
    int cur = 1;

    for (int i = 0; i < n; i++) {
        if (c[i] == 0) dfs(i, g, c, cur++); // просто обход из каждой вершины, которая ещё не принадлежит к существующим компонентам связности
    }

    for (int i = 0; i < n; i++) { // вывод
        cout << "v["<<i<<"] = "<< c[i] << "\n";
    }
}
/*
* В графе определить, к какой компоненте связности относится каждая из вершин.(т.е. по факту, найти разбиение графа на компоненты связности)
*/
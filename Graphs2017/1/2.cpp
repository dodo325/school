#include <bits/stdc++.h>
using namespace std;
// http://e-maxx.ru/algo/bfs

const int inf = 1e9; // машинная бесконечность

void bfs(int s, vector<vector<int>> &g, vector<int> &d, vector<int> &p) { // поиск в ширину 
    fill(d.begin(), d.end(), inf);
    queue<int> q;
    q.push(s); // стартовая вершина
    d[s] = 0;
    p[s] = -1;
    while (!q.empty()) {
        int f = q.front();
        q.pop();
        for (int to : g[f]) {
            if (d[to] == inf) {
                d[to] = d[f] + 1;
                p[to] = f;
                q.push(to);
            }
        }
    }
}

int main() {
    int n, m, a, b;
    cin >> n >> m >> a >> b;
    a--; b--;
    vector<vector<int>> g(n);
    for (int i = 0; i < m; i++) {
        int s, t;
        cin >> s >> t;
        s--; t--;
        g[s].push_back(t);
        g[t].push_back(s);
    }
    vector<int> d(n), // массив длин путей
                p(n); // массив "предков"
    bfs(b, g, d, p);
    int ans = d[a];
    if (ans == inf) {
        cout << -1;
        return 0;
    }
    cout << ans << endl;
    while (b != a) { // вывод наикратчайшего
        cout << a + 1 << " ";
        a = p[a];
    }
    cout << b + 1; 
}


/*
 * В графе найти кратчайший путь между заданной парой вершин
 * 	а) длину этого пути
 * 	б) сам путь, как последовательность вершин
 *
*/
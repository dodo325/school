#include <iostream>

using namespace std;

int main() {

    //Graph *g = new Graph();
  //  g->inputLE();
    int m=0,n=0;
    cout<<"number of nodes: "; cin>>n;
    cout<<"number of edges: "; cin>>m;

    int** V = new int *[n];
    for (int i = 0; i < n; ++i) {
        V[i] = new int[n];
        for (int j = 0; j < n; ++j) V[i][j] = 0;
    }


    int** E = new int *[m];
    for (int i = 0; i < m; ++i) {
        E[i] = new int[2];
        cin>>E[i][0]>> E[i][1];
        V[E[i][0]][E[i][1]] = V[E[i][1]][E[i][0]] = 1;
    }

    cout<<"Edges list: \n";
    for (int i = 0; i < m; ++i) {
        cout<<i<<": ( ";
        cout<< E[i][0]<<" ; ";
        cout<< E[i][1];
        cout<<") \n";
    }

    cout<<"\n Adjacency matrix: \n";
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << V[i][j] << "  ";
        }
        cout << "\n";
    }

    cout<<"\n Adjacency list: ";
    for (int i = 0; i < n; ++i) {
        cout << "\n " << i << ": ";
        for (int j = 0; j < n; ++j)
            if (V[i][j] == 1) cout << j << ",";
    }

    cout<<"\n Incidence matrix: ";
    int** imat = new int *[n];
    for (int i = 0; i < n; ++i) {
        imat[i] = new int[m];
        cout << "\n " ;
        for (int j = 0; j < m; ++j) {
            if(i == E[j][0] || i == E[j][1]) imat[i][j] = 1;
            else imat[i][j] = 0;
            cout << imat[i][j] << " ";
        }
    }
    return 0;
}
/*
class Graph {
    int m=0,n=0; // |V|=m ; |E|=n


public:
    Graph (){
        m=n=0;
    }

    void inputLE() {
        printf("Enter |E|: "); scanf("%d", &n);
         int *E = new int[n][2];
         for(int i = 0; i < n; i++)
            scanf("%d %d", E[i][0], E[i][1]); // = E[i]
        printf("aaa = %d", n);
    }

};*/

## Шпора для [полугодовой](https://docs.google.com/document/d/1davO3cU2HrFbQRQg5iQdFUyPIjN4_c_4fDIiKmAhSIE/edit)   ) 

1. (3 балла) Постройте граф с вершинной связностью - 3, реберной связностью - 4, и минимальной степенью вершины - 5, содержащий три компоненты вершинной 4 - связности. Решение требует обоснования.
    ### [гмм... но не так всё просто](http://e-maxx.ru/algo/connectivity_back_problem)


2. (3 балла) (3 балла) Напишите функцию вычисляющую число компонент графа построенного на N вершинах. Входные параметры функции: (int** G, int N), где G - матрицу смежности графа нужно считать уже заданной. Шапку программы, и int main() писать не нужно.
   ### [Что делать?](http://cppalgo.blogspot.ru/2012/08/blog-post.html)
   ### - не думать, списывать!

```c++
int aa(int** G, int N) {
  bool usd[N];
  for (int k=1;k<=n;++k){
    for (int i=1;i<=n;++i){
      for (int j=1;j<=n;++j){
        if (G[i][k] + G[k][j] == 2)    
          G[i][j] = 1;
      }
    }
  }
 
  int cnt = 0;
  for (int i=1;i<=n;++i) {
    if (!usd[i]) {
      ++cnt;
      usd[i] = true;
      for (int j=1;j<=n;++j) {
        if (G[i][j] && !usd[j])
          usd[j] = true;
      }
    }
  }
  return cnt; 
}
```
И плевать что O(N^3), хотя можно и [DFS](https://github.com/dodo325/school/blob/master/Graphs2017/1/1.cpp)

3. (4 балла) Напишите программу вычисляющую количество ребер не являющимися мостами в графе заданном списком ребер.
[e-maxx!!!](http://e-maxx.ru/algo/bridge_searching). В третьей искать [мосты](http://e-maxx.ru/algo/bridge_searching) и [шарниры](http://e-maxx.ru/algo/cutpoints), или их реализации: [мосты](https://github.com/OMS7/Informatics/blob/master/HW-22.11.2017/P2.cpp) и [шарниры](https://github.com/OMS7/Informatics/blob/master/HW-22.11.2017/P1.cpp)

4. В четвертой достаточно знать [Уоршелла-Флойда](http://e-maxx.ru/algo/floyd_warshall_algorithm), [Форда-Беллмана](http://e-maxx.ru/algo/ford_bellman) и [Прима](http://e-maxx.ru/algo/mst_prim). Например, [вот решение задачи из демки](https://gist.github.com/dodo325/eaa2e0753078caa039c7681bf10c1775). 





[+](https://github.com/OMS7/Informatics)

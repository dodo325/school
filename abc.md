## Что будет:
1.  [двумерный массив(на манер змейки)](https://github.com/dodo325/school/blob/master/DZ/6.cpp "моя хня")
2.  перебор/порядковая статистика
3.  жадность/динамика
4.  цепная структура 
5.  кодирование

## 2. [Поиск k-ой порядковой статистики](http://e-maxx.ru/algo/kth_order_statistics "Необязательная подсказка")
```cpp
int findOrderStatistic(int[] array, int k) {
  int left = 0, right = array.length;
  while (true) {
    int mid = partition(array, left, right);

    if (mid == k) {
      return array[mid];
    }
    else if (k < mid) {
      right = mid;s
    }
    else {
      k -= mid + 1;
      left = mid + 1;
    }
  }
}
```
## 3. Жадные алгоритмы:
+ [Рюкзак](https://neerc.ifmo.ru/wiki/index.php?title=%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%BE_%D1%80%D1%8E%D0%BA%D0%B7%D0%B0%D0%BA%D0%B5#.D0.A0.D0.B5.D0.B0.D0.BB.D0.B8.D0.B7.D0.B0.D1.86.D0.B8.D1.8F_3)
+ 

## Полезные плюшки:
+ [qsort vector](http://www.cplusplus.com/reference/algorithm/sort/ "EN!") and [qsort source](https://github.com/dodo325/school/blob/master/09.12/quicksort.cpp "my github")
+ Простота:
   ```cpp
   bool isP(int n) {
   	if (n == 0) return false;
  	bool isPrime = true;
  	for (int i = 2; i*i <= n ; ++i)a
  	{
  		if (n % i == 0)
  		{
  			isPrime = false;
  			break;
  		}
  	}
  	return isPrime;
  }
  ```
+ НОД:
  ```cpp
  int gcd(int a, int b) {
	if (b < 0) b = -b;
  if (a < 0) a = -a;
	if (b == 0 || a == 0) return max(a,b);
  
  if (a == b) return a;
  if (a > b) {
		  int tmp = a;
		  a = b;
		  b = tmp;
	  }
	  return gcd(a, b - a);
  }
  ```
+ [сумма цифр, перевернуть число...](https://github.com/dodo325/school/blob/master/11.01.17/DZ_na_zimnie_kanikuly_10-m_klass.pdf)

#include <bits/stdc++.h>
using namespace std;
int main() {
	int n;
	cin >> n;
	vector<vector<int>> mat(n, vector<int>(n));
	for (int i = 0; i < n*(n - 1) / 2; i++) {
		int a, b,c;
		cin >> a >> b>>c;
		a--; b--;
		mat[a][b] = mat[b][a] = c;
	}
	for (int k = 0; k < n; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
	
	int s = n * 10000;
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < n; j++) 
			s+=mat[i][j];
	cout << s;
}